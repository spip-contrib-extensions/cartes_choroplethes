<?php
/**
 * Définit les autorisations du plugin Cartes choroplèthes
 *
 * @plugin     Cartes choroplèthes
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Cartes_choroplethes\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function cartes_choroplethes_autoriser() {
}

/**
 * Autorisation de voir un élément de menu (pots)
 *
 * Fonction relative aux menus déroulant de l'interface privée
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/

function autoriser_pots_menu_dist($faire, $type, $id, $qui, $opt) {
	// l'admin peut tout
	if ($qui['statut'] == '0minirezo') return true;

	// pour les autres cas, on se fie à la déclaration de la constante
	$statut = AUTEURS_MIN_REDAC; // ceux qui peuvent écrire
	$statut = str_replace(',5poubelle', '', $statut); // on retire 5poubelle
	$statut = explode(',', $statut);

	return in_array($qui['statut'], $statut);
}
