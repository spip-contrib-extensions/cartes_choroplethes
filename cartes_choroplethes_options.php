<?php
/**
 * fonctions, variables et constantes nécessaires au plugin Cartes choroplèthes
 *
 * @plugin     Cartes choroplèthes
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Cartes_choroplethes\Options
 */
 

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

 	$GLOBALS['spip_pipeline']['gis_modele_parametres_autorises'] .= "|param_pots";

/**
 * Déclaration des arguments supplémentaires utilisables dans le modele pour gis
 *
 * L'argument pot pourra être passé au modèle :
 * les pots de peinture pourront être versés dans les squelettes de GIS...
 *
 * @param array $flux
 *     Arguments acceptés pour le modèle
 * @return array
 *     Arguments acceptés pour le modèle
 */

function param_pots($flux) {
		$flux[] = 'pot';
	return $flux;
}
