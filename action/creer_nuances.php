<?php

/**
 * Gestion de l'action pour obtenir des nuances à partir du pot de peinture
 *
 * @plugin     Cartes choroplèthes
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\cartes_choroplethes\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour créer les couleurs nécessaires à une carte choroplèthe
 *
 * L'argument attendu est `1-#F40404-5`
 * id_pot : numéro d'identifiant unique
 * couleur : couleur du thème, qui sera éclaircie pour avoir une palette de nuance.
 * quantile : nombre de classes de répartition pour les données (auxquelles seront affectées des couleurs).
 *
 * @uses action_contruire_palette_dist()
 * une fonction surchargeable permettant la colorisation
 *
 * @param null|string $arg
 *     Clé des arguments. 
 * @example : 1-#F40404-5
 *
 * @return void
 */
 
function action_creer_nuances_dist ($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	$arg = explode("-", $arg);
	list($pot, $couleur, $quantile) = $arg;

	// vérification des arguments

	if  ((!intval($pot)) OR (!$couleur))
		return;
		
	$mon_carte_coloriee = charger_fonction('contruire_palette', 'action');
	$resultat = $mon_carte_coloriee($pot);

	if (!$resultat) // on a un problème, il faut donc le tracer dans un log
		spip_log(_T('cartes_choroplethes_pots:log_colorier', array(
			'arg' => implode('-',$arg),
			'aut' => $GLOBALS['auteur_session']['id_auteur'],
			'pot' => $pot
			)), 'cartes_choroplethes.' . _LOG_INFO_IMPORTANTE);

}

 /**
 * Construit la palette selon le thème, les éléments de celui-ci et le quantile choisi
 *
 * @param integer $pot
 *     Identifiant unique du pot de peinture dans la table spip_cartes_choroplethes_pots
 * @param integer $quantile
 *     Nombre de parts dans la distribution des couleurs sur les valeurs
 * @return boolean true|false
 *     reussite de l'opération ou non
 */
	
function action_contruire_palette_dist($pot,$quantile = 5){
	// Combien de régions avons nous concernées par le thème ?
	$nbre_regions = sql_countsel('spip_cartes_choroplethes_nuances', array(
		"id_pot=" . intval($pot)),
		"id_region");
	// Combien de valeurs avons nous concernées par le thème ?
	$nbre_valeurs = sql_countsel('spip_cartes_choroplethes_nuances', array(
		"id_pot=" . intval($pot)));
	// si le nombre de régions équivaut au nombre de valeurs, nous ne sommes pas dans une logique de ratio ou d'opération
	// on peut directement calculer les quantiles.
	if ($nbre_regions = $nbre_valeurs) {
		// Tout d'abord les valeurs doivent être triées par ordre croissant.
		// (order by va le trier par ordre alphabétique et non pas numérique. 
		// Il faut une astuce : 'LENGTH(valeur), valeur')
		$valeurs = sql_allfetsel('valeur,id_nuance','spip_regions_nuances', array(
			"id_pot=" . intval($pot)),'','LENGTH(valeur), valeur');
		if ($nbre_valeurs >= $quantile) {
			// l'ensemble des données est divisible dans le nombre des parties souhaitée.
			// Chacune d’entre elles est appelée quintile.
			$nbre_valeurs_par_quantile = $nbre_valeurs / $quantile;
			// on va chercher la couleur du pot
			$couleur_du_pot = sql_getfetsel('couleur', 'spip_cartes_choroplethes_pots', "id_pot=" . intval($pot));
			$palette_couleurs = array();
			$couleur = substr($couleur_du_pot, 1); // supression du # qui sera ajouté par la suite.
			// on utiliser les fonctions de SPIP pour éclaircir la couleur en 4 nuances plus claires
			include_spip('inc/filtres_images_mini');
			$i=4;
			while ($i >= 0) {
				$palette_couleurs[$i] =  "#" .  $couleur;
 				$couleur = couleur_eclaircir($palette_couleurs[$i], 0.3);
   				$i--;
			}
			// on repartit la couleur du thème par quantile
			// la base, claire, prend la valeur supplémentaire (s'il y a)
			$i = 0; $nbr_quantile = 0; 
			$marche_inf = floor($nbre_valeurs_par_quantile); $marche_sup = ceil($nbre_valeurs_par_quantile);
			$reste_a_distribuer = $nbre_valeurs_par_quantile - $marche_inf;
			$reste_a_distribuer = floor($reste_a_distribuer * $quantile);
			// le quantile représentant les valeurs les plus faibles bénéficie du reste à distribuer (arbitraire esthétique)
			$marche_sup = $marche_inf + $reste_a_distribuer;
			while ($i <= $nbre_valeurs - 1) {
				if ($i < $marche_sup) {
					$valeurs[$i]['couleur'] = $palette_couleurs[$nbr_quantile];
				} 
				if ($i >= $marche_sup AND $i < $marche_sup + $marche_inf){
					$valeurs[$i]['couleur'] = $palette_couleurs[$nbr_quantile + 1];
				}
				if ($i >= $marche_sup + $marche_inf AND $i < $marche_sup + ($marche_inf * 2)){
					$valeurs[$i]['couleur'] = $palette_couleurs[$nbr_quantile + 2];
				}
				if ($i >= $marche_sup + ($marche_inf * 2) AND $i < $marche_sup + ($marche_inf * 3)){
					$valeurs[$i]['couleur'] = $palette_couleurs[$nbr_quantile + 3];
				} 
				if ($i >= $marche_sup + ($marche_inf * 3) AND $i < $marche_sup + ($marche_inf * 4)){
					$valeurs[$i]['couleur'] = $palette_couleurs[$nbr_quantile + 4];
				}   
				if ($i >= $marche_sup + ($marche_inf * 4) AND $i < $marche_sup + ($marche_inf * 5)){
					$valeurs[$i]['couleur'] = $palette_couleurs[$nbr_quantile + 5];
				}   
				$i++;
			}			
		} else return false;
	} else return false;
	
	// insérer les couleurs de chaque élément dans la table
	foreach ($valeurs as $valeur) {
     	$retour = sql_updateq('spip_cartes_choroplethes_nuances', 
    		array('couleur' => $valeur['couleur']),
    		"id_nuance=".$valeur['id_nuance']
    	);
    	if (!$retour) return false;
 	}
	return true;
}
