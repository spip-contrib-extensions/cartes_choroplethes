<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin Cartes choroplethes
 *
 * @plugin     Cartes choroplethes
 * @copyright  2019-2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Cartes_choroplethes\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Fonction d'installation et de mise à jour du plugin Cartes choroplethes.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function cartes_choroplethes_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	include_spip('base/importer_spip_cartes_choroplethes_pots');
	include_spip('base/importer_spip_cartes_choroplethes_nuances');
	$maj['create'] = array(
		array('maj_tables', array('spip_cartes_choroplethes_pots','spip_cartes_choroplethes_nuances')),
		array('importer_spip_cartes_choroplethes_pots'),
		array('importer_spip_cartes_choroplethes_nuances')
 	);

 	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);

}


/**
 * Fonction de désinstallation du plugin Cartes choroplethes.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
**/
function cartes_choroplethes_vider_tables($nom_meta_base_version) {

	sql_drop_table('spip_cartes_choroplethes_pots');
	sql_drop_table('spip_cartes_choroplethes_nuances');


	effacer_meta($nom_meta_base_version);
}
