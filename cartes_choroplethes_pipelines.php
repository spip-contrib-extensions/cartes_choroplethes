<?php
/**
 * Définit les pipelines utilisés par le plugin Cartes choroplèthes
 *
 * @plugin     Cartes choroplèthes
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Cartes_choroplethes\Pipelines
 */

if (!defined("_ECRIRE_INC_VERSION")) return;


/**
 * Ajout de contenu sur certaines pages,
 * notamment des formulaires de liaisons entre objets
 *
 * @pipeline affiche_milieu
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function cartes_choroplethes_affiche_milieu($flux) {
	$texte = '';
	$e = trouver_objet_exec($flux['args']['exec']);

	// Auteurs associés aux pots de peintures 
	// (pour, éventuellement, donnez des rôles aux auteurs vis-à-vis des thèmes cartographiques)
	if (!$e['edition'] and in_array($e['type'], array('carte_choroplethe_pot'))) {
		$texte .= recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'auteurs',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']],
	        'editable'=>autoriser('associerauteurs',$e['type'],$flux['args'][$e['id_table_objet']])?'oui':'non'
		));
	}

	// Subdivisions associées à une nuance 
	// (la forme géométrique aura alors la couleur de la nuance)
	if (!$e['edition'] and in_array($e['type'], array('carte_choroplethe_nuance'))
		and test_plugin_actif('subdivisions') ) {
		$texte .= recuperer_fond('prive/objets/editer/liens', array(
			'table_source' => 'subdivisions',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']],
	        'editable'=>autoriser('associersubdivisions',$e['type'],$flux['args'][$e['id_table_objet']])?'oui':'non'
		));
	}

	if ($texte) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	return $flux;
}


/**
 * Ajouter les objets sur les vues des parents directs
 *
 * Affiche les Objets cartes_choroplethes_nuances, enfant de carte_choroplethe_pot :
 * (On donne les nuances issues du pot de peinture...)
 *
 * @pipeline affiche_enfants
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
**/
function cartes_choroplethes_affiche_enfants($flux) {
	if ($e = trouver_objet_exec($flux['args']['exec']) and $e['edition'] == false) {
		$id_objet = $flux['args']['id_objet'];
		if ($e['type'] == 'carte_choroplethe_pot') {
			if (autoriser('creercarte_choroplethe_nuancedans', 'cartes_choroplethes_pots', $id_objet)) {
				include_spip('inc/presentation');
				$flux['data'] .= icone_verticale(
					_T('cartes_choroplethes_nuances:icone_creer_nuance'),
					generer_url_ecrire('carte_choroplethe_nuance_edit', "id_pot=$id_objet"),
					'carte_choroplethe_nuance-24.png',
					'new',
					'right'
				) . "<br class='nettoyeur' />";
			}
			if (autoriser('voir', 'cartes_choroplethes_nuances')) {
				$flux['data'] .= recuperer_fond(
					'prive/objets/liste/cartes_choroplethes_nuances',
					array(
						'titre' => _T('cartes_choroplethes_nuances:titre_nuances_du_pot'),
						'id_pot' => $id_objet,
						'numpot' => 'desactive'
					)
				);
			}
		}
	}
	return $flux;
}


?>