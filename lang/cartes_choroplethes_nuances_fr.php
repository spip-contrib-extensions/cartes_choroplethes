<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E

	'erreur_valeur_deja_presente' =>"Une valeur a déjà été accordée à la région ! Pour l'instant les cartes sont avec une seule couche de donnée possible.",
	
	// L

	'label_valeur' => "Valeur",
	'label_valeur_explication' => "Valeur numérique.",
	'label_etiquette' => "Étiquette",
	'label_etiquette_explication' => "Votre étiquette est polyglotte, c'est à dire sous la forme d'une balise HTML &lt;multi&gt; qui propose des nuances multilingues.",
	'label_couleur' => "Couleur",
	'label_couleur_explication' => "La couleur des nuances du thème est dérivée de la couleur du thème. Mais il vous est possible de <i>forcer</i> cette couleur en la renseignant ici.",
	
	// I

	'icone_supprimer_nuance' => "Supprimer cette nuance",
	'icone_creer_nuance' => "Créer une nouvelle nuance",
	'icone_modifier_nuance' => "Modifier la nuance",
	'info_1_nuance' => "Une nuance",
	'info_aucune_nuance' => "Aucune nuance !",
	'info_nb_nuances' => "@nb@ nuances",

	// T

	'titre_nuances_du_pot' => "Nuances de la couleur du pot de peinture",
	'titre_langue_nuance' => "Langue de la nuance",
	'titre_logo_nuance' => "Logo de la nuance",
	'titre_nuance' => "Nuance",
	'titre_nuances' => "Nuances",
	'texte_nouvel_nuance' => "Nouvelle nuance",

);

?>
