<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A

	'alt_couleur_export' => "Exportation des couleurs",
	
	// L

	'label_description' => "Description",
	'label_titre' => "Titre",
	'label_couleur' => "Couleur",
	'label_couleur_explication' => "Il s'agit de la couleur de votre pot de peinture, à choisir <b>foncée</b>. En effet, le logiciel va l'éclaircir, pour vous proposer une palette de nuance à partir de cette couleur.",
	'label_quantile' => "Quantile",
	'label_quantile_explication' => "L'ensemble des données du thème est divisé en parties égales, de sorte que chaque partie contienne le même nombre de valeurs. Les seuils entre ces parties sont appelés <b>quantiles</b>. Vous aurez ainsi, dans la mesure du possible, une carte aux couleurs harmonieusement réparties. Il faut au moins deux couleurs (et au moins deux données auxquelles les appliquer).",
	'log_colorier' => "action_colorier_elements_dist : La demande de colorisation des éléments par l'auteur n°@aut@ du thème n°@theme@ avec les arguments @arg@ n'a pas pu aboutir.",
	
	// I

	'icone_creer_pot' => "Créer un nouveau pot",
	'icone_modifier_pot' => "Modifier le pot de peinture",
	'info_1_pot' => "Un pot de peinture",
	'info_aucun_pot' => "Pas de pot (de peinture) !",
	'info_nb_pots' => "@nb@ pots de peinture",
	'info_numero_pot' => "N° Pot",

	// T

	'titre_verser' => "Versez la couleur et créez les nuances",
	'texte_changer_statut' => "Ce pot de peinture est :",
	'titre_langue_pot' => "Langue du pot",
	'titre_logo_pot' => "Logo du pot",
	'titre_pot' => "Pot de peinture",
	'titre_pots' => "Pots de peinture",

);

?>
