<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'cartes_choroplethes_description' => 'Un plugin qui s\'utilise avec GIS et GIS Géométrie pour proposer des cartes choroplèthes. (Faire facilement des cartes des densités de population, des résultats électoraux, des performances de la culture des cucurbitacées...)',
	'cartes_choroplethes_nom' => 'Cartes choroplèthes',
	'cartes_choroplethes_slogan' => 'De la peinture pour vos cartes ! ',
);
