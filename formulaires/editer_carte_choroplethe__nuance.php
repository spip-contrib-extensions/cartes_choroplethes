<?php
/**
 * Gestion du formulaire d'édition d'une nuance
 *
 * @plugin     Cartes choroplèthes
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\cartes_choroplethes_\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Un simple formulaire,
 * on a juste à déclarer les saisies et le plugin Saisies pour Formulaire facilite le reste.
 *
 * @param void 
 * @return array
 *     Tableau du formulaire
 */
function nuance_saisies($icone, $fieldset_text){

  $mes_saisies = array(
  array( // le fieldset du thème
    'saisie' => 'fieldset',
    'options' => array(
      'nom' => 'fieldset_pot',
      'label' => $fieldset_text,
      'icone' => $icone,
    ),
     'saisies' => array( // les champs dans le fieldset 
       array( // savoir d'où l'on vient
          'saisie' => 'hidden',
          'options' => array(
            'nom' => 'retour',
           )
      ),
       array( // champ id_nuance (numéro unique de l'élément du thème)
          'saisie' => 'hidden',
          'options' => array(
            'nom' => 'id_nuance',
           )
      ),
      array( 
          'saisie' => 'cartes_choroplethes_pots',
          'options' => array(
            'nom' => 'id_pot',
            'label' => _T('cartes_choroplethes_pots:titre_pot'),
 			'obligatoire' => 'oui',
 			'cacher_option_intro' => 'oui',
           ),
      ),
      array( 
          'saisie' => 'textarea',
          'options' => array(
            'nom' => 'etiquette',
            'label' => _T('cartes_choroplethes_nuances:label_etiquette'),
            'explication' => _T('cartes_choroplethes_nuances:label_etiquette_explication'),
			'obligatoire' => 'oui',
 			'conteneur_class' => 'pleine_largeur',
 			'placeholder' => '&lt;multi&gt;[fr]en français[en]in english</multi>',
			//'inserer_barre' => 'non',
			//'previsualisation' => 'non',
          ),
      ),
      array( 
          'saisie' => 'input',
          'options' => array(
            'nom' => 'valeur',
            'label' => _T('cartes_choroplethes_nuances:label_valeur'),
            'explication' => _T('cartes_choroplethes_nuances:label_valeur_explication'),
  			'conteneur_class' => 'pleine_largeur',
 			//'inserer_barre' => 'non',
			//'previsualisation' => 'non',
         ),
 		'verifier' => array(
			'type' => 'decimal'
		)
    ),
       array( // champ couleur (peut être forcée)
          'saisie' => 'couleur',
          'options' => array(
            'nom' => 'couleur',
           'label' => _T('cartes_choroplethes_nuances:label_couleur'),
           'explication' => _T('cartes_choroplethes_nuances:label_couleur_explication'),
            //'class' =>'palette'
           ),
		'verifier' => array(
			'type' => 'couleur',
			'options' => array(
				'hexa'
			)
		)

       ),
 )));
  return $mes_saisies;
} 


/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet édité
 *
 * @param int|string $id_nuance
 *     Identifiant unique de l'élément. 'new' pour un nouvel élément.
 * @param int $id_pot
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un élément source d'une traduction (inutile car on est en multi)
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de la table de l'objet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_carte_choroplethe_nuance_identifier_dist($id_nuance = 'new', $id_pot = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_nuance)));
}

/**
 * Chargement du formulaire d'édition d'un élément d'un thème
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_nuance
 *     Identifiant unique de l'élément. 'new' pour un nouvel élément.
 * @param int $id_pot
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un élément source d'une traduction (inutile car on est en multi)
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de la table de l'objet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_carte_choroplethe_nuance_charger_dist($id_nuance = 'new', $id_pot = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {

	$valeurs = formulaires_editer_objet_charger('carte_choroplethe_nuance', $id_nuance, $id_pot, $lier_trad, $retour, $config_fonc, $row, $hidden);
	
	// cas d'une création nouvelle
	if (!intval($valeurs['id_nuance'])) {
		$valeurs['id_nuance'] = $id_nuance;
		$valeurs['id_pot'] = $id_pot;
		// donner un exemple aux rédacteurs peu habitués aux Polyglottes.
		$valeurs['etiquette'] = "<multi>[fr]L'étiquette en français[en]Label in english</multi>"; 
		$icone = "carte_choroplethe_nuance-new-24";
		$fieldset_text = _T('cartes_choroplethes_nuances:icone_creer_nuance');
	} else {
		$icone = "carte_choroplethe_nuance-edit-24";
		$fieldset_text = _T('cartes_choroplethes_nuances:icone_modifier_nuance');
	}
	$valeurs['retour'] = $retour;

	// transmettre les listes au plugin Saisies pour formulaires

	$valeurs['_mes_saisies'] = nuance_saisies($icone,$fieldset_text);
	
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition d'un élément d'un thème
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_nuance
 *     Identifiant unique de l'élément. 'new' pour un nouvel élément.
 * @param int $id_pot
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un élément source d'une traduction (inutile car on est en multi)
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de la table de l'objet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_carte_choroplethe_nuance_verifier_dist($id_nuance = 'new', $id_pot = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
 // on va chercher le pipeline saisies_verifier() dans son fichier
  include_spip('inc/saisies');
  // on récupère les saisies
  $mes_saisies = nuance_saisies();
  // saisies_verifier retourne un tableau des erreurs s'il y en a, sinon traiter() prend le relais
  return saisies_verifier($mes_saisies);

}

/**
 * Traitement du formulaire d'édition d'un élément d'un thème
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *     fonction de l'API « editer_objet »
 * @param int|string $id_nuance
 *     Identifiant unique de l'élément. 'new' pour un nouvel élément.
 * @param int $id_pot
 *     Identifiant de l'objet parent (si connu)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un élément source d'une traduction (inutile car on est en multi)
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de la table de l'objet, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_carte_choroplethe_nuance_traiter_dist($id_nuance = 'new', $id_pot = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {


	$retours = formulaires_editer_objet_traiter(
		'carte_choroplethe_nuance', 
		$id_nuance, 
		$id_pot, 
		$lier_trad, 
		$retour, 
		$config_fonc, 
		$row, 
		$hidden
	);

	return $retours;
}
