<?php
/**
 * Gestion du formulaire de d'édition d'un pot
 *
 * @plugin     Cartes choroplèthes
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Cartes_choroplethes\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Un simple formulaire de config,
 * on a juste à déclarer les saisies
 **/
function pot_saisies(){

  $mes_saisies = array(
  array( // le fieldset du pot de peinture
    'saisie' => 'fieldset',
    'options' => array(
      'nom' => 'fieldset_pot',
      'label' => _T('cartes_choroplethes_pots:label_fieldset_pot'),
      'icone' => 'carte_choroplethe_pot-24',
    ),
     'saisies' => array( // les champs dans le fieldset 
       array( // savoir d'où l'on vient
          'saisie' => 'hidden',
          'options' => array(
            'nom' => 'retour',
           )
      ),
      array( 
          'saisie' => 'textarea',
          'options' => array(
            'nom' => 'titre',
            'label' => _T('cartes_choroplethes_pots:label_titre'),
 			'obligatoire' => 'oui',
 			'conteneur_class' => 'pleine_largeur',
 			'placeholder' => '&lt;multi&gt;[fr]en français[en]in english</multi>',
 			//'inserer_barre' => 'non',
			//'previsualisation' => 'non',
          ),
      ),
      array( 
          'saisie' => 'textarea',
          'options' => array(
            'nom' => 'description',
            'label' => _T('cartes_choroplethes_pots:label_description'),
  			'conteneur_class' => 'pleine_largeur',
 			'placeholder' => '&lt;multi&gt;[fr]en français[en]in english</multi>',
 			//'inserer_barre' => 'non',
			//'previsualisation' => 'non',
         ),
     ),
       array( 
          'saisie' => 'couleur',
          'options' => array(
            'nom' => 'couleur',
           'label' => _T('cartes_choroplethes_pots:label_couleur'),
           'explication' => _T('cartes_choroplethes_pots:label_couleur_explication'),
            //'class' =>'palette'
           ),
		'verifier' => array(
			'type' => 'couleur',
			'options' => array(
				'hexa'
			)
		)

       ),
       array( 
          'saisie' => 'input',
          'options' => array(
            'nom' => 'quantile',
           'label' => _T('cartes_choroplethes_pots:label_quantile'),
           'explication' => _T('cartes_choroplethes_pots:label_quantile_explication'),
            //'class' =>'palette'
           ),
		'verifier' => array(
			'type' => 'entier',
			'options' => array(
				'min' => 2,
				'max' => 10
			)
		)
       ),
 )));
  return $mes_saisies;
} 


/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet édité
 *
 * @param int|string $id_pot
 *     Identifiant du pot de peinture. 'new' pour un nouveau pot.
 * @param int $id_parent
 *     Identifiant de l'objet parent (si connu) (non pertinent, pas de parent)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un pot source d'une traduction (non pertinent, on est en multi)
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du pot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_carte_choroplethe_pot_identifier_dist($id_pot = 'new', $id_parent = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	return serialize(array(intval($id_pot)));
}

/**
 * Chargement du formulaire d'édition d'un pot de peinture
 *
 * Déclarer les champs postés et y intégrer les valeurs par défaut
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_pot
 *     Identifiant du pot de peinture. 'new' pour un nouveau pot.
 * @param int $id_parent
 *     Identifiant de l'objet parent (si connu) (non pertinent, l'objet n'ayant pas de parent)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un pot source d'une traduction (non pertinent, l'objet étant en multi)
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du pot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 */
function formulaires_editer_carte_choroplethe_pot_charger_dist($id_pot = 'new', $id_parent = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$valeurs = formulaires_editer_objet_charger('carte_choroplethe_pot', $id_pot, $id_parent, $lier_trad, $retour, $config_fonc, $row, $hidden);
	if (!$valeurs['id_pot']) {
		$valeurs['id_pot'] = $id_pot;
	}
	$valeurs['retour'] = $retour;

	// transmettre les listes au plugin Saisies pour formulaires

	$valeurs['_mes_saisies'] = pot_saisies();
	
	return $valeurs;
}

/**
 * Vérifications du formulaire d'édition du thème
 *
 * Vérifier les champs postés et signaler d'éventuelles erreurs
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_pot
 *     Identifiant du pot de peinture. 'new' pour un nouveau pot.
 * @param int $id_parent
 *     Identifiant de l'objet parent (si connu) (non pertinent, l'objet n'ayant pas de parent)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un pot source d'une traduction (non pertinent, l'objet étant en multi)
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du pot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Tableau des erreurs
 */
function formulaires_editer_carte_choroplethe_pot_verifier_dist($id_pot = 'new', $id_parent = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
 // on va chercher le pipeline saisies_verifier() dans son fichier
  include_spip('inc/saisies');
  // on récupère les saisies
  $mes_saisies = pot_saisies();
  // saisies_verifier retourne un tableau des erreurs s'il y en a, sinon traiter() prend le relais
  return saisies_verifier($mes_saisies);

}

/**
 * Traitement du formulaire d'édition du thème
 *
 * Traiter les champs postés
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_pot
 *     Identifiant du pot de peinture. 'new' pour un nouveau pot.
 * @param int $id_parent
 *     Identifiant de l'objet parent (si connu) (non pertinent car l'objet n'a pas de parent)
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un pot source d'une traduction (non pertinent car l'objet est en multi)
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du pot, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 */
function formulaires_editer_carte_choroplethe_pot_traiter_dist($id_pot = 'new', $id_parent = 0, $retour = '', $lier_trad = 0, $config_fonc = '', $row = array(), $hidden = '') {
	$retours = formulaires_editer_objet_traiter('carte_choroplethe_pot', $id_pot, $id_parent, $lier_trad, $retour, $config_fonc, $row, $hidden);
	return $retours;
}
