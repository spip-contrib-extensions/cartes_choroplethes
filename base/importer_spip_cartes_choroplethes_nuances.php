<?php
#
# Ces fichiers sont a placer dans le repertoire base/ de votre plugin
#
/**
 * Gestion de l'importation de `spip_cartes_choroplethes_nuances`
 * Gestion des liaisons entre la table `spip_cartes_choroplethes_nuances` et `spip_subdivisions_liens`
 *
**/

/**
 * Fonction d'import de la table `spip_cartes_choroplethes_nuances`
 * à utiliser dans le fichier d'administration du plugin
 *
 *     ```
 *     include_spip('base/importer_spip_cartes_choroplethes_nuances');
 *     $maj['create'][] = array('importer_spip_cartes_choroplethes_nuances');
 *     ```
 *
**/
function importer_spip_cartes_choroplethes_nuances() {
	######## VERIFIEZ LE NOM DE LA TABLE D'INSERTION ###########
	$table = 'spip_cartes_choroplethes_nuances';
	$table_liaison = 'spip_subdivisions_liens';
	$colonne_id_table_liaison = 'id_subdivision';
	$colonne_objet = 'carte_choroplethe_nuance';
	$colonne_id_objet = 'id_nuance';
	// nom_du_champ_source => nom_du_champ_destination
	// mettre vide la destination ou supprimer la ligne permet de ne pas importer la colonne.

	$correspondances = array(
		'id_subdivision' => 'id_subdivision',
		'id_nuance' => 'id_nuance',
		'id_pot' => 'id_pot',
		'etiquette' => 'etiquette',
		'valeur' => 'valeur',
		'couleur' => 'couleur'
	);
		

	// transposer les donnees dans la nouvelle structure
	$inserts = array();
	$inserts_liaisons = array();
	list($cles, $valeurs) = donnees_spip_cartes_choroplethes_nuances();
	// on remet les noms des cles dans le tableau de valeur
	// en s'assurant de leur correspondance au passage
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) {
			$i = array();$j = array();
			foreach ($v as $k => $valeur) {
				$cle = $cles[$k];
				if (isset($correspondances[$cle]) and $correspondances[$cle]) {
					// on récupère l'info de liaison
					if ($cle == $colonne_id_table_liaison) {
						$j[ $correspondances[$cle] ] = $valeur;
						$j[ 'id_objet' ] = $i[$colonne_id_objet];
						$j[ 'objet' ] = $colonne_objet;
						$inserts_liaisons[] = $j;
						spip_log("liaison projetée de : "
							. $correspondances[$cle] . '=' . $j[ $correspondances[$cle] ]
							. '|' . 'id_objet' . '=' . $j[ 'id_objet' ] 
							. '|' . 'objet' . '=' . $colonne_objet, 'cartes_choroplethes.' . _LOG_INFO_IMPORTANTE);
					} else	
						$i[ $correspondances[$cle] ] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs);

		// inserer les donnees en base.
		$nb_inseres = 0;
		// ne pas reimporter ceux deja la (en cas de timeout)
		$nb_deja_la = sql_countsel($table);
		$inserts = array_slice($inserts, $nb_deja_la);
		$nb_a_inserer = count($inserts);
		// on decoupe en petit bout (pour reprise sur timeout)
		$inserts = array_chunk($inserts, 100);
		foreach ($inserts as $i) {
			sql_insertq_multi($table, $i);
			$nb_inseres += count($i);
			// serie_alter() relancera la fonction jusqu'a ce que l'on sorte sans timeout.
			if (time() >= _TIME_OUT) {
				// on ecrit un gentil message pour suivre l'avancement.
				echo "<br />Insertion dans $table relanc&eacute;e : ";
				echo "<br />- $nb_deja_la &eacute;taient d&eacute;j&agrave; l&agrave;";
				echo "<br />- $nb_inseres ont &eacute;t&eacute; ins&eacute;r&eacute;s.";
				$a_faire = $nb_a_inserer - $nb_inseres;
				echo "<br />- $a_faire &agrave; faire.";
				return;
			}
		}
		// inserer les liaisons en base.
		$nb_inseres = 0;
		$nb_a_inserer = count($inserts_liaisons);
		// on decoupe en petit bout (pour reprise sur timeout)
		$inserts_liaisons = array_chunk($inserts_liaisons, 100);
		spip_log("liaisons faites : " . print_r($inserts_liaisons,true), 'cartes_choroplethes.' . _LOG_INFO_IMPORTANTE);
		foreach ($inserts_liaisons as $i) {
			sql_insertq_multi($table_liaison, $i);
			$nb_inseres += count($i);
			// serie_alter() relancera la fonction jusqu'a ce que l'on sorte sans timeout.
			if (time() >= _TIME_OUT) {
				// on ecrit un gentil message pour suivre l'avancement.
				echo "<br />Insertion dans $table relanc&eacute;e : ";
				echo "<br />- $nb_deja_la &eacute;taient d&eacute;j&agrave; l&agrave;";
				echo "<br />- $nb_inseres ont &eacute;t&eacute; ins&eacute;r&eacute;s.";
				$a_faire = $nb_a_inserer - $nb_inseres;
				echo "<br />- $a_faire &agrave; faire.";
				return;
			}
		}
	}
}


/**
 * Données de la table spip_cartes_choroplethes_nuance
 *
 * id_subdivision : colonne fictive nécessaire pour l'association des deux tables : 
 *                  liaison de la subdivision à la nuance qui la coloriera
 * id_pot : parent, le pot de peinture (d'où proviennent les nuances)
 * id_nuance : identifiant unique de la nuance dans la table des nuances
 * etiquette : nom de la valeur	(ex : habitants)
 * valeur :	valeur (ex : 10 000)
 * couleur : la couleur est normalement une nuance de celle du thème, mais il est possible de la forcer.
 *
**/

function donnees_spip_cartes_choroplethes_nuances() {

	// identifier les régions françaises, qui seront mises en valeur dans la carte choroplèthe
	// par le code ISO de façon à avoir l'id_subdivision 
	$code_iso = array('FR-GUA','FR-MTQ','FR-GUF','FR-LRE','FR-MAY','FR-IDF','FR-CVL','FR-BFC','FR-NOR','FR-HDF','FR-GES','FR-PDL','FR-BRE','FR-NAQ','FR-OCC','FR-ARA','FR-PAC','FR-COR');
	
	$in = sql_in('iso_moi', $code_iso);

	$mon_id = array(); $completude = array();
	if ($lignes = sql_allfetsel('id_subdivision', 'spip_subdivisions', array($in))) {
		foreach ($lignes as $l) {
				$mon_id[] = $l['id_subdivision'];
		}
	} else {
	 	spip_log("function donnees_spip_cartes_choroplethes_nuances. requete refusée.",
	 				'cartes_choroplethes.' . _LOG_ERREUR);
	 	return;
	}
	
	
	$cles = array('id_pot', 'id_nuance', 'id_subdivision',  'etiquette', 'valeur', 'couleur' );

// Thème 1 pour exemple : population 2020 des régions
// Source numérique : insee Estimation de population par région au 1er janvier 2020.
// https://www.insee.fr/fr/statistiques/1893198

	$valeurs = array(
		array( '1', '1', $mon_id[0], '<multi>[fr]habitants[en]inhabitants</multi>', '376879', '#c2c2e7'), // Guadeloupe
		array( '1', '2', $mon_id[1], '<multi>[fr]habitants[en]inhabitants</multi>', '358749', '#c2c2e7'), // Martinique
		array('1', '3', $mon_id[2], '<multi>[fr]habitants[en]inhabitants</multi>', '290691', '#c2c2e7'), // Guyane
		array('1', '4', $mon_id[3], '<multi>[fr]habitants[en]inhabitants</multi>', '859959', '#c2c2e7'), // La Réunion
		array('1', '5', $mon_id[4], '<multi>[fr]habitants[en]inhabitants</multi>', '279471', '#c2c2e7'), // Mayotte
		array('1', '6', $mon_id[5], '<multi>[fr]habitants[en]inhabitants</multi>', '12278210', '#000099'), // Île-de-France
		array('1', '7', $mon_id[6], '<multi>[fr]habitants[en]inhabitants</multi>', '2559073', '#a8a8dc'), // Centre-Val de Loire
		array('1', '8', $mon_id[7], '<multi>[fr]habitants[en]inhabitants</multi>', '2783039', '#a8a8dc'), // Bourgogne-Franche-Comté
		array('1', '9', $mon_id[8], '<multi>[fr]habitants[en]inhabitants</multi>', '3303500', '#a8a8dc'), // Normandie
		array('1', '10', $mon_id[9], '<multi>[fr]habitants[en]inhabitants</multi>', '5962662', '#4d4db8'), // Hauts-de-France
		array('1', '11', $mon_id[10], '<multi>[fr]habitants[en]inhabitants</multi>', '5511747', '#4d4db8'), // Grand Est
		array('1', '12', $mon_id[11], '<multi>[fr]habitants[en]inhabitants</multi>', '3801797', '#8282cd'), // Pays de la Loire
		array('1', '13', $mon_id[12], '<multi>[fr]habitants[en]inhabitants</multi>', '3340379', '#8282cd'), // Bretagne
		array('1', '14', $mon_id[13], '<multi>[fr]habitants[en]inhabitants</multi>', '5999982', '#000099'), // Nouvelle-Aquitaine
		array('1', '15', $mon_id[14], '<multi>[fr]habitants[en]inhabitants</multi>', '5924858', '#4d4db8'), // Occitanie
		array('1', '16', $mon_id[15], '<multi>[fr]habitants[en]inhabitants</multi>', '8032377', '#000099'), // Auvergne-Rhône-Alpes
		array('1', '17', $mon_id[16], '<multi>[fr]habitants[en]inhabitants</multi>', '5055651', '#8282cd'), // Région Sud (PACA)
		array('1', '18', $mon_id[17], '<multi>[fr]habitants[en]inhabitants</multi>', '344679', '#c2c2e7') // Corse
	);


	return array($cles, $valeurs);
}