<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     Cartes choroplèthes
 * @copyright  2020
 * @author     Vincent CALLIES
 * @licence    GNU/GPL
 * @package    SPIP\Cartes_choroplethes\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function cartes_choroplethes_declarer_tables_interfaces($interfaces) {

	$interfaces['table_des_tables']['cartes_choroplethes_pots'] = 'cartes_choroplethes_pots';
	$interfaces['table_des_tables']['cartes_choroplethes_nuances'] = 'cartes_choroplethes_nuances';
	
	$interfaces['table_des_traitements']['DESCRIPTION']['cartes_choroplethes_pots']= _TRAITEMENT_TYPO;
	$interfaces['table_des_traitements']['ETIQUETTE']['cartes_choroplethes_nuances']= _TRAITEMENT_TYPO;
 	
 	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * La table spip_cartes_choroplethes_pots est le conteneur.
 * Vous pouvez la voir comme un pot de peinture.
 *
 * La table spip_cartes_choroplethes_nuances
 * sont les nuances de la peinture sur le terrain.
 * Vous verserez un pot de peinture sur un terrain plus ou moins absorbant ce qui fera des nuances.
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function cartes_choroplethes_declarer_tables_objets_sql($tables) {

	$tables['spip_cartes_choroplethes_pots'] = array(
		'texte_retour' => 'icone_retour',
		'texte_objets' => 'cartes_choroplethes_pots:titre_pots',
		'texte_objet' => 'cartes_choroplethes_pots:titre_pot',
		'texte_modifier' => 'cartes_choroplethes_pots:icone_modifier_pot',
		'texte_creer' => 'cartes_choroplethes_pots:icone_creer_pot',
		'info_aucun_objet'=> 'cartes_choroplethes_pots:info_aucun_pot',
		'info_1_objet' => 'cartes_choroplethes_pots:info_1_pot',
		'info_nb_objets' => 'cartes_choroplethes_pots:info_nb_pots',
		'texte_logo_objet' => 'cartes_choroplethes_pots:titre_logo_pot',
		'texte_langue_objet' => 'cartes_choroplethes_pots:titre_langue_pot',
		'type' => 'carte_choroplethe_pot',
		'principale' => 'oui',
		'icone_objet' => 'carte_choroplethe_pot',
		'field'=> array(
			'id_pot'	=> 'bigint(21) NOT NULL',
			'titre' => 'text NOT NULL',
			'description' => 'text NOT NULL',
			'couleur' => "varchar(7) NOT NULL DEFAULT ''",
			'quantile' => "tinyint NOT NULL DEFAULT '2'",
			'date' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			'statut' => "varchar(255) DEFAULT '0' NOT NULL",
			'maj' => "TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP"
		),
		'key' => array(
			'PRIMARY KEY' => 'id_pot'
		),
		'titre' => 'titre AS titre, "" AS lang',
		'date' => "date",
		'champs_editables'  => array('titre', 'description', 'couleur', 'quantile'),
		'champs_versionnes' => array('titre', 'description', 'couleur', 'quantile'),
		'rechercher_champs' => array("titre" => 1, "description" => 8),
		#'tables_jointures' => array('id_pot => cartes_choroplethes_nuances'),
		'statut'=> array(
			array(
				'champ' => 'statut',
				'publie' => 'publie',
				'previsu' => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'statut_textes_instituer' => 	array(
			'prepa' => 'texte_statut_en_cours_redaction',
			'prop' => 'texte_statut_propose_evaluation',
			'publie' => 'texte_statut_publie',
			'refuse' => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle'
		),
		'texte_changer_statut' => 'cartes_choroplethes_pots:texte_changer_statut',		
	);
	
	$tables['spip_cartes_choroplethes_nuances'] = array(
		'texte_retour' => 'icone_retour',
		'texte_objets' => 'cartes_choroplethes_nuances:titre_nuances',
		'texte_objet' => 'cartes_choroplethes_nuances:titre_nuance',
		'texte_modifier' => 'cartes_choroplethes_nuances:icone_modifier_nuance',
		'texte_creer' => 'cartes_choroplethes_nuances:icone_creer_nuance',
		'info_aucun_objet'=> 'cartes_choroplethes_nuances:info_aucun_nuance',
		'info_1_objet' => 'cartes_choroplethes_nuances:info_1_nuance',
		'info_nb_objets' => 'cartes_choroplethes_nuances:info_nb_nuances',
		'texte_logo_objet' => 'cartes_choroplethes_nuances:titre_logo_nuance',
		'texte_langue_objet' => 'cartes_choroplethes_nuances:titre_langue_nuance',
		'type' => 'carte_choroplethe_nuance',
		'principale' => 'oui',
		'icone_objet' => 'carte_choroplethe_nuance',
		'field'=> array(
			'id_nuance'	=> 'bigint(21) NOT NULL',
			'id_pot' => 'bigint(21) NOT NULL DEFAULT 0',
			'etiquette' => 'text NOT NULL',
			'valeur' => 'text NOT NULL',
			'couleur' => "varchar(7) NOT NULL DEFAULT ''"
		),
		'key' => array(
			'PRIMARY KEY' => 'id_nuance',
			'KEY'	=> 'id_pot',
		),
		'titre' => 'etiquette AS titre, "" AS lang',
		#'date' => "date",
		'champs_editables'  => array('id_pot', 'etiquette', 'valeur', 'couleur'),
		'rechercher_champs' => array("etiquette" => 1),
		'parent' => array('type' => 'cartes_choroplethes_pots', 'champ' => 'id_pot'), //plugin declarerparent
		#'tables_jointures' => 'cartes_choroplethes_nuances_liens',
	);

 return $tables;
}

