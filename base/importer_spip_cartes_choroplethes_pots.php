<?php
#
# Ces fichiers sont a placer dans le repertoire base/ de votre plugin
#
/**
 * Gestion de l'importation de `spip_cartes_choroplethes_pots`
**/

/**
 * Fonction d'import de la table `spip_cartes_choroplethes_pots`
 * à utiliser dans le fichier d'administration du plugin
 *
 *     ```
 *     include_spip('base/importer_spip_cartes_choroplethes_pots');
 *     $maj['create'][] = array('importer_spip_cartes_choroplethes_pots');
 *     ```
 *
**/
function importer_spip_cartes_choroplethes_pots() {
	######## VERIFIEZ LE NOM DE LA TABLE D'INSERTION ###########
	$table = 'spip_cartes_choroplethes_pots';

	// nom_du_champ_source => nom_du_champ_destination
	// mettre vide la destination ou supprimer la ligne permet de ne pas importer la colonne.

	$correspondances = array(
		'id_pot' => 'id_pot',
		'titre' => 'titre',
		'description' => 'description', 
		'couleur' => 'couleur', 
		'quantile' => 'quantile', 
		'date' => 'date', 
		'statut' => 'statut', 
		'maj' => 'maj' 
	);
		

	// transposer les donnees dans la nouvelle structure
	$inserts = array();
	list($cles, $valeurs) = donnees_spip_cartes_choroplethes_pots();
	// on remet les noms des cles dans le tableau de valeur
	// en s'assurant de leur correspondance au passage
	if (is_array($valeurs)) {
		foreach ($valeurs as $v) {
			$i = array();
			foreach ($v as $k => $valeur) {
				$cle = $cles[$k];
				if (isset($correspondances[$cle]) and $correspondances[$cle]) {
					$i[ $correspondances[$cle] ] = $valeur;
				}
			}
			$inserts[] = $i;
		}
		unset($valeurs);

		// inserer les donnees en base.
		$nb_inseres = 0;
		// ne pas reimporter ceux deja la (en cas de timeout)
		$nb_deja_la = sql_countsel($table);
		$inserts = array_slice($inserts, $nb_deja_la);
		$nb_a_inserer = count($inserts);
		// on decoupe en petit bout (pour reprise sur timeout)
		$inserts = array_chunk($inserts, 100);
		foreach ($inserts as $i) {
			sql_insertq_multi($table, $i);
			$nb_inseres += count($i);
			// serie_alter() relancera la fonction jusqu'a ce que l'on sorte sans timeout.
			if (time() >= _TIME_OUT) {
				// on ecrit un gentil message pour suivre l'avancement.
				echo "<br />Insertion dans $table relanc&eacute;e : ";
				echo "<br />- $nb_deja_la &eacute;taient d&eacute;j&agrave; l&agrave;";
				echo "<br />- $nb_inseres ont &eacute;t&eacute; ins&eacute;r&eacute;s.";
				$a_faire = $nb_a_inserer - $nb_inseres;
				echo "<br />- $a_faire &agrave; faire.";
				return;
			}
		}
	}
}


/**
 * Donnees de la table spip_cartes_choroplethes_pots
 *
 * id_pot : numéro d'identifiant unique du pot de peinture
 * titre : titre au format multi (division par langue)
 * description : description au format multi 
 * couleur : couleur du pot ; c'est à partir de cette couleur que les dégradés (plus clairs) seront réalisés.
 *			La couleur est indiquée en hexadécimal (ne pas oublier le #)
 * date :	Date de création du pot.
 * statut :	statut du pot, qui peut devenir obsolète (pas de pot !).
 * maj :	date de la mise à jour du pot.
 *
**/

function donnees_spip_cartes_choroplethes_pots() {

	$cles = array('id_pot', 'titre', 'description', 'couleur', 'quantile', 'date', 'statut', 'maj' );


	$valeurs = array(
		array('1', '<multi>[fr]Population[en]Population</multi>', '<multi>[fr]Répartition de la population par régions françaises en 2020[en]Distribution of the population by French regions in 2020</multi>', '#000099', '5', '2020:01:01 00:00:00', 'publie','')
	);


	return array($cles, $valeurs);
}